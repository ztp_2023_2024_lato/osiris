package com.paluchp.pk.ztp.osiris.internal.exceptions;

public class OsirisException extends RuntimeException {

  public OsirisException(String message) {
    super(message);
  }

  public OsirisException(String message, Throwable cause) {
    super(message, cause);
  }
}
