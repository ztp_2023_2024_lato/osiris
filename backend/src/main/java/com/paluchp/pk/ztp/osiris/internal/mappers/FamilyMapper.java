package com.paluchp.pk.ztp.osiris.internal.mappers;

import com.paluchp.pk.ztp.osiris.internal.entities.FamilyEntity;
import com.paluchp.pk.ztp.osiris.model.FamilyCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.FamilyListResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyUpdateRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface FamilyMapper {
  @Mapping(target = "id", ignore = true)
  FamilyEntity toEntity(FamilyCreateRequestDto familyCreateRequestDto);

  FamilyResponseDto toResponse(FamilyEntity entity);

  List<FamilyResponseDto> toResponse(List<FamilyEntity> entity);

  default FamilyListResponseDto toListResponse(List<FamilyEntity> entity) {
    return new FamilyListResponseDto().items(toResponse(entity));
  }

  @Mapping(target = "id", ignore = true)
  void updateEntity(@MappingTarget FamilyEntity entity, FamilyUpdateRequestDto familyUpdateRequestDto);
}
