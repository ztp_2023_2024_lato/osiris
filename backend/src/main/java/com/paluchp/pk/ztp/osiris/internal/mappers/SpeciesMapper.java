package com.paluchp.pk.ztp.osiris.internal.mappers;

import com.paluchp.pk.ztp.osiris.internal.entities.SpeciesEntity;
import com.paluchp.pk.ztp.osiris.model.SpeciesCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesListResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesUpdateRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface SpeciesMapper {
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "family", ignore = true)
  SpeciesEntity toEntity(SpeciesCreateRequestDto speciesCreateRequest);

  SpeciesResponseDto toResponse(SpeciesEntity persisted);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "family", ignore = true)
  void updateEntity(@MappingTarget SpeciesEntity entity, SpeciesUpdateRequestDto speciesUpdateRequestDto);

  List<SpeciesResponseDto> toResponse(List<SpeciesEntity> list);

  default SpeciesListResponseDto toListResponse(List<SpeciesEntity> all) {
    return new SpeciesListResponseDto().items(toResponse(all));
  }

}
