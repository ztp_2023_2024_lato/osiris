package com.paluchp.pk.ztp.osiris.api.family;

import com.paluchp.pk.ztp.osiris.api.FamilyApi;
import com.paluchp.pk.ztp.osiris.internal.services.FamilyService;
import com.paluchp.pk.ztp.osiris.model.FamilyCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.FamilyListResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class FamilyController implements FamilyApi {

  private final FamilyService service;

  @Override
  public ResponseEntity<FamilyResponseDto> createFamily(FamilyCreateRequestDto familyCreateRequestDto) {
    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(service.createFamily(familyCreateRequestDto));
  }

  @Override
  public ResponseEntity<Void> deleteFamily(UUID id) {
    service.deleteFamily(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @Override
  public ResponseEntity<FamilyListResponseDto> getFamilies() {
    return ResponseEntity.ok(service.getFamilies());
  }

  @Override
  public ResponseEntity<FamilyResponseDto> getFamilyById(UUID id) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(service.getFamilyById(id));
  }

  @Override
  public ResponseEntity<FamilyResponseDto> updateFamily(UUID id, FamilyUpdateRequestDto familyUpdateRequestDto) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(service.updateFamily(id, familyUpdateRequestDto));
  }
}
