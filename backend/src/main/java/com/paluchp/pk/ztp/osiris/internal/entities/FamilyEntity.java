package com.paluchp.pk.ztp.osiris.internal.entities;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Getter
@Setter
@Entity
@Table(name = "family")
public class FamilyEntity extends BaseEntity {

  @Column
  private String name;

  @Column
  private String description;
}
