package com.paluchp.pk.ztp.osiris.internal.exceptions;

import com.paluchp.pk.ztp.osiris.model.ConstraintViolationResponseDto;
import com.paluchp.pk.ztp.osiris.model.ExceptionResponseDto;
import com.paluchp.pk.ztp.osiris.model.ViolationDto;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  private static ViolationDto mapFieldErrors(ConstraintViolation<?> violation) {
    return new ViolationDto()
        .fieldName(violation.getPropertyPath().toString())
        .message(violation.getMessage());
  }

  @ExceptionHandler(OsirisException.class)
  public ResponseEntity<ExceptionResponseDto> handleOsirisException(OsirisException ex) {
    log.error("Osiris exception: {}", ex.getMessage());
    ResponseStatus status = ex.getClass().getAnnotation(ResponseStatus.class);

    int statusCode = 500;
    if (status != null) {
      statusCode = status.value().value();
      if (statusCode == 500) {
        statusCode = status.code().value();
      }
    }

    long timestamp = System.currentTimeMillis();
    return ResponseEntity.status(statusCode)
        .body(new ExceptionResponseDto().message(ex.getMessage()).status(statusCode).timestamp(timestamp));
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<Object> handleValidationException(ConstraintViolationException ex) {
    log.info("Constrain violation exception: {}", ex.getMessage());
    List<ViolationDto> violations =
        ex.getConstraintViolations()
            .stream()
            .map(GlobalExceptionHandler::mapFieldErrors)
            .toList();
    return createConstraintViolationResponse(violations);
  }

  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity<ExceptionResponseDto> handleFallback(RuntimeException ex) {
    log.error("Runtime exception", ex);
    long timestamp = System.currentTimeMillis();
    return ResponseEntity.status(500)
        .body(new ExceptionResponseDto().message(ex.getMessage()).status(500).timestamp(timestamp));
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatusCode status,
      WebRequest request) {
    log.info("Constrain violation exception");
    List<ViolationDto> violations =
        ex.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(this::mapFieldError)
            .toList();

    return createConstraintViolationResponse(violations);
  }

  private ViolationDto mapFieldError(FieldError fieldError) {
    return new ViolationDto()
        .fieldName(fieldError.getField())
        .message(fieldError.getDefaultMessage());
  }

  private ResponseEntity<Object> createConstraintViolationResponse(List<ViolationDto> violations) {
    ConstraintViolationResponseDto responseDto =
        new ConstraintViolationResponseDto().violations(violations);

    return ResponseEntity.badRequest().body(responseDto);
  }
}
