package com.paluchp.pk.ztp.osiris.internal.services;

import com.paluchp.pk.ztp.osiris.internal.entities.FamilyEntity;
import com.paluchp.pk.ztp.osiris.internal.mappers.FamilyMapper;
import com.paluchp.pk.ztp.osiris.internal.repositories.FamilyRepository;
import com.paluchp.pk.ztp.osiris.internal.utils.ResponseHelper;
import com.paluchp.pk.ztp.osiris.model.FamilyCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.FamilyListResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyResponseDto;
import com.paluchp.pk.ztp.osiris.model.FamilyUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class FamilyService {

  private final FamilyRepository repo;
  private final FamilyMapper mapper;

  public FamilyResponseDto createFamily(FamilyCreateRequestDto familyCreateRequestDto) {
    FamilyEntity entity = mapper.toEntity(familyCreateRequestDto);
    FamilyEntity persisted = repo.save(entity);
    return mapper.toResponse(persisted);
  }

  public void deleteFamily(UUID id) {
    if (!repo.existsById(id)) {
      throw ResponseHelper.notFound(id, FamilyEntity.class).get();
    }
    repo.deleteById(id);
  }

  public FamilyResponseDto getFamilyById(UUID id) {
    return repo
        .findById(id)
        .map(mapper::toResponse)
        .orElseThrow(ResponseHelper.notFound(id, FamilyEntity.class));
  }

  public FamilyResponseDto updateFamily(UUID id, FamilyUpdateRequestDto familyUpdateRequestDto) {
    FamilyEntity entity = repo.findById(id).orElseThrow(ResponseHelper.notFound(id, FamilyEntity.class));
    mapper.updateEntity(entity, familyUpdateRequestDto);
    FamilyEntity persisted = repo.save(entity);
    return mapper.toResponse(persisted);
  }

  public FamilyListResponseDto getFamilies() {
    return mapper.toListResponse(repo.findAll());
  }
}
