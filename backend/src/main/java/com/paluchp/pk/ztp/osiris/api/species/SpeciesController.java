package com.paluchp.pk.ztp.osiris.api.species;

import com.paluchp.pk.ztp.osiris.api.SpeciesApi;
import com.paluchp.pk.ztp.osiris.internal.services.SpeciesService;
import com.paluchp.pk.ztp.osiris.model.SpeciesCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesListResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class SpeciesController implements SpeciesApi {

  private final SpeciesService service;

  @Override
  public ResponseEntity<SpeciesResponseDto> createSpecies(SpeciesCreateRequestDto speciesCreateRequest) {
    return ResponseEntity.status(HttpStatus.CREATED).body(service.createSpecies(speciesCreateRequest));
  }

  @Override
  public ResponseEntity<Void> deleteSpecies(UUID id) {
    service.deleteSpecies(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @Override
  public ResponseEntity<SpeciesResponseDto> getSpeciesById(UUID id) {
    return ResponseEntity.status(HttpStatus.OK).body(service.getSpeciesById(id));
  }

  @Override
  public ResponseEntity<SpeciesListResponseDto> listSpecies() {
    return ResponseEntity.ok(service.listSpecies());
  }

  @Override
  public ResponseEntity<SpeciesResponseDto> updateSpecies(UUID id, SpeciesUpdateRequestDto speciesUpdateRequestDto) {
    return ResponseEntity.status(HttpStatus.OK).body(service.updateSpecies(id, speciesUpdateRequestDto));
  }
}
