package com.paluchp.pk.ztp.osiris.internal.utils;

import com.paluchp.pk.ztp.osiris.internal.exceptions.ResponseNotFoundException;
import lombok.experimental.UtilityClass;

import java.util.UUID;
import java.util.function.Supplier;

@UtilityClass
public class ResponseHelper {
  public static Supplier<ResponseNotFoundException> notFound(UUID id, Class<?> entityClass) {
    return () -> new ResponseNotFoundException(id, entityClass);
  }
}
