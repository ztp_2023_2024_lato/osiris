package com.paluchp.pk.ztp.osiris.internal.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
@Profile("!test")
public class AuthFilter extends OncePerRequestFilter {

  public static final String AUTH_COOKIE = "auth";

  private final JwtService jwtService;

  public static ResponseCookie createAuthCookie(String token) {
    ResponseCookie.ResponseCookieBuilder cookie = ResponseCookie.from(AUTH_COOKIE, token);
    cookie.secure(true);
    cookie.httpOnly(true);
    cookie.path("/");
    cookie.maxAge((int) (JwtService.getExpirationTime() * 1000));
    return cookie.build();
  }

  private static Optional<Cookie> getAuthCookie(Cookie[] cookies) {
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(AUTH_COOKIE)) {
          return Optional.of(cookie);
        }
      }
    }
    return Optional.empty();
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    Cookie[] cookies = request.getCookies();
    getAuthCookie(cookies)
        .flatMap(this::validateJWT)
        .ifPresentOrElse(s -> authUser(s, request, response), this::clearAuth);

    filterChain.doFilter(request, response);
  }

  private void authUser(String subject, HttpServletRequest request, HttpServletResponse response) {
    UsernamePasswordAuthenticationToken authenticate = new UsernamePasswordAuthenticationToken(subject, null, List.of());
    authenticate.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    SecurityContextHolder.getContext().setAuthentication(authenticate);

    // new cookie to refresh session time
    ResponseCookie springCookie = createAuthCookie(jwtService.generateToken(authenticate));
    Cookie cookie = new Cookie(springCookie.getName(), springCookie.getValue());
    cookie.setSecure(springCookie.isSecure());
    cookie.setHttpOnly(springCookie.isHttpOnly());
    cookie.setPath(springCookie.getPath());
    cookie.setMaxAge((int) springCookie.getMaxAge().toSeconds());
    response.addCookie(cookie);
  }

  private void clearAuth() {
    SecurityContextHolder.clearContext();
  }

  private Optional<String> validateJWT(Cookie cookie) {
    String jwtString = cookie.getValue();
    return jwtService.validateToken(jwtString);
  }
}
