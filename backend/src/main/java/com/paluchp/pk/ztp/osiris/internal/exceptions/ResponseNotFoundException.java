package com.paluchp.pk.ztp.osiris.internal.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResponseNotFoundException extends OsirisException {
  public ResponseNotFoundException(UUID id) {
    super("Not found entity with id " + id);
  }

  public ResponseNotFoundException(UUID id, Class<?> entityClass) {
    super("Not found " + entityClass.getSimpleName() + " with id " + id);
  }
}
