package com.paluchp.pk.ztp.osiris.internal.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "species")
public class SpeciesEntity extends BaseEntity {

  @Column
  private String name;

  @Column
  private String description;

  @JoinColumn(name = "family_id")
  @ManyToOne
  private FamilyEntity family;
}