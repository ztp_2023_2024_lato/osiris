package com.paluchp.pk.ztp.osiris.api.auth;

import com.paluchp.pk.ztp.osiris.api.AuthApi;
import com.paluchp.pk.ztp.osiris.internal.security.AuthFilter;
import com.paluchp.pk.ztp.osiris.internal.security.JwtService;
import com.paluchp.pk.ztp.osiris.model.LoginRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@Profile("!test")
public class AuthController implements AuthApi {

  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;

  @Override
  public ResponseEntity<Void> login(LoginRequestDto loginRequest) {
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);

    String token = jwtService.generateToken(authentication);
    ResponseCookie cookie = AuthFilter.createAuthCookie(token);

    return ResponseEntity
        .status(HttpStatus.OK)
        .header(HttpHeaders.SET_COOKIE, cookie.toString())
        .build();
  }
}
