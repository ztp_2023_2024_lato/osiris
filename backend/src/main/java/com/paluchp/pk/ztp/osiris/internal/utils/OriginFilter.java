package com.paluchp.pk.ztp.osiris.internal.utils;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CorsFilter;

import java.io.IOException;

@Component
public class OriginFilter extends CorsFilter {

  public OriginFilter() {
    super(r -> null);
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    String origin = request.getHeader(HttpHeaders.ORIGIN);
    response.setHeader("Access-Control-Allow-Origin", origin);
    response.setHeader("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
    response.setHeader("Access-Control-Allow-Headers", "*");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Max-Age", "180");

    if(request.getMethod().equals(HttpMethod.OPTIONS.name())){
      String requestedAllowHeaders = request.getHeader("Access-Control-Request-Headers");
      response.setHeader("Access-Control-Allow-Headers", requestedAllowHeaders);
      response.setStatus(200);
    }

    filterChain.doFilter(request, response);
  }

}