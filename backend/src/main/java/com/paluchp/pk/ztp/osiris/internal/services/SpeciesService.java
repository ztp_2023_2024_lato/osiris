package com.paluchp.pk.ztp.osiris.internal.services;

import com.paluchp.pk.ztp.osiris.internal.entities.FamilyEntity;
import com.paluchp.pk.ztp.osiris.internal.entities.SpeciesEntity;
import com.paluchp.pk.ztp.osiris.internal.mappers.SpeciesMapper;
import com.paluchp.pk.ztp.osiris.internal.repositories.FamilyRepository;
import com.paluchp.pk.ztp.osiris.internal.repositories.SpeciesRepository;
import com.paluchp.pk.ztp.osiris.internal.utils.ResponseHelper;
import com.paluchp.pk.ztp.osiris.model.SpeciesCreateRequestDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesListResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesResponseDto;
import com.paluchp.pk.ztp.osiris.model.SpeciesUpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class SpeciesService {

  private final SpeciesMapper mapper;
  private final SpeciesRepository repo;
  private final FamilyRepository familyRepo;

  public SpeciesResponseDto createSpecies(SpeciesCreateRequestDto speciesCreateRequest) {
    SpeciesEntity entity = mapper.toEntity(speciesCreateRequest);
    UUID familyId = speciesCreateRequest.getFamily();
    FamilyEntity family =
        familyRepo.findById(familyId)
            .orElseThrow(ResponseHelper.notFound(familyId, FamilyEntity.class));

    entity.setFamily(family);
    SpeciesEntity persisted = repo.save(entity);
    return mapper.toResponse(persisted);
  }

  public SpeciesResponseDto getSpeciesById(UUID id) {
    return repo
        .findById(id)
        .map(mapper::toResponse)
        .orElseThrow(ResponseHelper.notFound(id, SpeciesEntity.class));
  }

  public void deleteSpecies(UUID id) {
    if (!repo.existsById(id)) {
      throw ResponseHelper.notFound(id, SpeciesEntity.class).get();
    }
    repo.deleteById(id);
  }

  public SpeciesResponseDto updateSpecies(UUID id, SpeciesUpdateRequestDto speciesUpdateRequestDto) {
    SpeciesEntity entity = repo.findById(id).orElseThrow(ResponseHelper.notFound(id, SpeciesEntity.class));
    mapper.updateEntity(entity, speciesUpdateRequestDto);
    UUID newFamilyId = speciesUpdateRequestDto.getFamily();
    if (!newFamilyId.equals(entity.getFamily().getId())) {
      FamilyEntity family =
          familyRepo.findById(newFamilyId)
              .orElseThrow(ResponseHelper.notFound(newFamilyId, FamilyEntity.class));
      entity.setFamily(family);
    }
    SpeciesEntity updated = repo.save(entity);
    return mapper.toResponse(updated);
  }

  public SpeciesListResponseDto listSpecies() {
    return mapper.toListResponse(repo.findAll());
  }
}
