package com.paluchp.pk.ztp.osiris.internal.repositories;

import com.paluchp.pk.ztp.osiris.internal.entities.SpeciesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface SpeciesRepository extends CrudRepository<SpeciesEntity, UUID> {

  @Override
  List<SpeciesEntity> findAll();
}
