package com.paluchp.pk.ztp.osiris.internal.security;

import io.jsonwebtoken.Jwts;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Optional;

@Service
@Profile("!test")
public class JwtService {

  @Getter
  private static long expirationTime;

  private final SecretKey key;

  public JwtService() {
    key = Jwts.SIG.HS512.key().build();
  }

  @SuppressWarnings("PMD")
  @Value("${osiris.security.expirationTime:360}") //...
  private void setExpirationTime(long expirationTime) {
    JwtService.expirationTime = expirationTime;
  }

  public String generateToken(Authentication authentication) {
    long now = System.currentTimeMillis();

    return
        Jwts.builder()
            .subject(authentication.getName())
            .issuedAt(new Date(now))
            .expiration(new Date(now + expirationTime * 1000))
            .signWith(key)
            .compact();
  }


  public Optional<String> validateToken(String token) {
    try {
      String subject =
          Jwts.parser().verifyWith(key).build().parseSignedClaims(token).getPayload().getSubject();
      return Optional.of(subject);
    } catch (RuntimeException er) {
      return Optional.empty();
    }
  }
}
