package com.paluchp.pk.ztp.osiris.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paluchp.pk.ztp.osiris.internal.entities.FamilyEntity;
import com.paluchp.pk.ztp.osiris.internal.entities.SpeciesEntity;
import com.paluchp.pk.ztp.osiris.internal.repositories.FamilyRepository;
import com.paluchp.pk.ztp.osiris.internal.repositories.SpeciesRepository;
import com.paluchp.pk.ztp.osiris.model.SpeciesCreateRequestDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SpeciesTests {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private SpeciesRepository repo;
  @MockBean
  private FamilyRepository familyRepo;

  @Test
  void shouldReturnCreatedStatusForValidDto() throws Exception {
    // given
    when(familyRepo.findById(Mockito.any(UUID.class)))
        .thenReturn(Optional.of(new FamilyEntity()));
    when(repo.save(Mockito.any(SpeciesEntity.class)))
        .thenAnswer(invocation -> invocation.getArgument(0));

    SpeciesCreateRequestDto dto =
        new SpeciesCreateRequestDto()
            .name("test")
            .description("desc")
            .family(UUID.fromString("00000000-0000-0000-0000-000000000000"));

    // when
    mockMvc.perform(
            post("/species")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(dto)))
        // then
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.name").value("test"))
        .andExpect(jsonPath("$.description").value("desc"));
  }
}
