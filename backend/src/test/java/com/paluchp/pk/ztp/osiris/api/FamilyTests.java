package com.paluchp.pk.ztp.osiris.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paluchp.pk.ztp.osiris.internal.entities.FamilyEntity;
import com.paluchp.pk.ztp.osiris.internal.repositories.FamilyRepository;
import com.paluchp.pk.ztp.osiris.model.FamilyCreateRequestDto;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FamilyTests {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private FamilyRepository familyRepository;

  @Nested
  class FamilyCreate {
    @Test
    void shouldReturnCreatedStatusForValidDto() throws Exception {
      when(familyRepository.save(Mockito.any(FamilyEntity.class)))
          .thenAnswer(invocation -> invocation.getArgument(0));

      FamilyCreateRequestDto dto =
          new FamilyCreateRequestDto()
              .name("test")
              .description("desc");

      mockMvc.perform(
              post("/family")
                  .contentType("application/json")
                  .content(objectMapper.writeValueAsString(dto)))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.name").value("test"))
          .andExpect(jsonPath("$.description").value("desc"));
    }

    @Test
    void shouldReturnBadRequestStatusForWrongDto() throws Exception {
      mockMvc.perform(
              post("/family")
                  .contentType("application/json")
                  .content("{}"))
          .andExpect(status().isBadRequest());
    }
  }

  @Nested
  class FamilyGet {
    @Test
    void shouldReturnOkStatusForValidDto() throws Exception {
      mockMvc.perform(get("/family"))
          .andExpect(status().isOk());
    }

    @Test
    void shouldReturnNotFoundStatusForInvalidUuid() throws Exception {
      mockMvc.perform(get("/family/00000000-0000-0000-0000-000000000000"))
          .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnOkStatusWithFamilies() throws Exception {
      when(familyRepository.findAll()).thenReturn(List.of(
          new FamilyEntity().setName("test1").setDescription("desc1"),
          new FamilyEntity().setName("test2").setDescription("desc2")
      ));
      mockMvc.perform(get("/family"))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.items[0].name").value("test1"))
          .andExpect(jsonPath("$.items[0].description").value("desc1"))
          .andExpect(jsonPath("$.items[1].name").value("test2"))
          .andExpect(jsonPath("$.items[1].description").value("desc2"));
    }
  }

}
