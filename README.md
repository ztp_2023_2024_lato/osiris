# Osiris
Aplikacja do przeglądania bazy roślin. 

- [Osiris](#osiris)
- [Backend](#backend)
  - [Uruchomienie](#uruchomienie)
  - [Budowanie](#budowanie)
  - [Struktura projektu](#struktura-projektu)
  - [OpenAPI](#openapi)
  - [Liquibase](#liquibase)
  - [MapStruct](#mapstruct)
  - [Security](#security)
  - [Testy](#testy)
  - [Linter](#linter)
- [Fronetend](#fronetend)
  - [Uruchomienie](#uruchomienie-1)
  - [Budowanie](#budowanie-1)
  - [Struktura projektu](#struktura-projektu-1)
  - [API](#api)
  - [Linter](#linter-1)
  - [Testy](#testy-1)
- [Endpointy](#endpointy)
- [GitLab CI/CD](#gitlab-cicd)

# Backend
Folder: [./backend`](./backend/)

Backend jest napisany w Spring-boot w Java 21. System budowania: maven

## Uruchomienie
Uruchomić bazę danych (dostępny docker-compose.yaml z profilem `db`). Konfiguracja bazy znajduje się w `src/java/resources/application.yaml`.

```shell
mvn spring-boot:run
```

## Budowanie
Wynikowy plik `.jar` znajduje się w folderze `target`.
```shell
mvn package
```
Dostępny jest [`docker-compose.yaml`](./backend/docker-compose.yaml) oraz [`Dockerfile`](./backend/Dockerfile) do zbudowania obrazu dockerowego.

## Struktura projektu

- `main/` 
  - `java/`
    - PAKIET_APLIKACJI
      - `api/`
        - `auth/` - Controllery auth
        - `family`- Controllery family
        - `species`- Controllery species
      - `internal`
        - `entity/` - Encje bazy danych
        - `exception/` - Wyjątki i ich obsługa
        - `mapper/` - Mappery dto <-> entity 
        - `repository/` - Repozytoria do obsługi bazy danych
        - `service/` - Serwisy do obsługi logiki biznesowej
        - `security/` - Konfiguracja security, tokentów, autoryzacji
        - `utils/` - Klasy narzędziowe
  - `resources/`
    - `db/` - Struktura liquibase
    - `openapi` - pliki openapi (swagger)
    - `application.yaml` - konfiguracja spring boot aplikacji
- `test/`
  - `java/` - kod testów
  - `resources/` 
    - `application.yaml` - konfiguracja spring boot do testów (przejście na bazę h2)

## OpenAPI
Projekt posiada w konfiguracji OpenAPI Generator, który przetwarza pliki `.yaml` z folderu `resources/openapi` na klasy dto oraz interfejsy klientów do komunikacji z backendem. Następnie kontrolery aplikacji implementują te interfejsy. W wypadku rozbudowy do struktury mikroserwisów jest możliwe wygenerowanie klientów do komunikacji między serwisami. 

OpenAPI jest uniwersalnym formatem i może być wykorzystywany do generowania dokumentacji czy funkcji requestów na frontendzie.

## Liquibase
Projekt korzysta z Liquibase do zarządzania migracjami bazy danych. Struktura migracji znajduje się w folderze `resources/db`. Pozwala to utrzymanie spójności bazy danych między środowiskami oraz wersjami aplikacji.

## MapStruct
Biblioteka MapStruct do Javy pozwala zapewnić separację między encjami bazy danych a DTO. Autmatycznie generuje mappery między tymi klasami równocześnie pozwalając na prostą konfigurację.

## Security
Spring Security zapewnia autoryzację i autentykację użytkowników. Projekt jest skonfigurowany tak by odrzucać wszystkie niezalogowane żądania. Po zalogowaniu użytkownik otrzymuje token JWT, który musi być przekazywany w ciasteczkach, skąd Spring Security go wyciąga, weryfikuje i pozwala na dostęp do zasobów dostarczając do serwisów w kontekście springowym danych o użytkowniku.

## Testy
Projekt posiada testy integracyjne. Sprawdzają poprawność działania całej aplikacji wraz z bazą danych. Testy korzystają z bazy danych H2, która jest tworzona na czas testów. Niektóre testy wykorzystują mocki do testowania wybranych fragmentów aplikacji.

## Linter
Używany jest PMD.




# Fronetend
Folder: [./frontend`](./frontend/)

Frontend jest napisany w Svelte. Z użyciem TypeScript i Less.

## Uruchomienie
```shell
npm install
npm run dev
```

## Budowanie
```shell
npm run build
```
Wybudowany projekt znajduje się w folderze `dist`.

## Struktura projektu

- `public/` - pliki statyczne
- `src/` - kod źródłowy
  - `assets/` - pliki zasobów
  - `components/` - komponenty svelte
  - `lib/` - moduły wykorzystywane przez komponenty
    - `api.ts` - klient do komunikacji z backendem
    - `auth.ts` - moduł autoryzacji
    - `mockData.ts` - dane testowe
    - `myStore.ts` - przechowywanie danych między komponentami
    - `types.ts` - globalne typy danych
    - `utils.ts` - funkcje pomocnicze
  - `app.less` - główny plik stylów
  - `App.svelte` - główny komponent aplikacji
  - `main.js` - plik wejściowy aplikacji
  - `theme.less` - zmienne stylów

## API
Funkcje api są w pliku [src/lib/api.ts`](./frontend/src/lib/api.ts).

Moduł wystawia funkcje pozwalajace komponentom wygodnie komunikować się z backendem po przez wywołanie funkcji przeznaczonej do określonego zadania i otrzymaniu Promise z wynikiem tego zadania, np. `function addSpecies(species: SpeciesCreateRequest) : ApiResponse<Species>`.

Pod spodem wykorzystywany jest `fetch` z automatyczną obsługą błędów komunikacji i serwera, opakowujacy wszystkie responsy w własny typ odpowiedzi by ujednolicić obsługę wszystkich responsów (również błędów) przez komponenty.

## Linter
Używany jest ESLint.

## Testy 
Używany jest Jest.





# Endpointy
Dokumentacja dostępna w [`api_docs.json`](./api_docs.html.json) oraz wersja wizualna w [`api_docs.html`](./api_docs.html).




# GitLab CI/CD
Pipeline są podzielone na 3 stage:
- lint
  - backend_lint
  - frontend_lint
- test
  - backend_test
  - frontend_test
- deploy
  - docker_deploy
  - frontend_deploy
  
Zadania są ustawione tak by się nie blokowały nawzajem czyli:
```
backend_lint -> backend_test   --.
                                 |--> deploy
frontend_lint -> frontend_test --'
```

Deploy jest wykonywany frontendu na firebase. 
Backend jest budowany do dockera.