module.exports = {
    root: true,
    env: {
      browser: true,
      es2021: true
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
      project: ['./tsconfig.json'],
      extraFileExtensions: ['.svelte']
    },
    extends: [
      'eslint:recommended',
      'plugin:svelte/recommended',
      'plugin:@typescript-eslint/recommended',
      './eslint-style.cjs',
      './eslint-svelte.cjs'
    ],
    plugins: [
      '@typescript-eslint'
    ],
    overrides: [
      {
        files: ['*.svelte'],
        parser: 'svelte-eslint-parser',
        parserOptions: {
          parser: '@typescript-eslint/parser'
        }
      }
    ]
  }