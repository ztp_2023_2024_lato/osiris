module.exports = {
  rules: {
    // Disable no-unused-vars in .svelte files
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "off",
    // disable style lang="less" error
    "svelte/block-lang": "off",
    // disable any error // TODO: remove this
    "@typescript-eslint/no-explicit-any": "off",
    // Require spacing in a HTML comment
    'svelte/spaced-html-comment': 'error',
    // Disable svelte a11y warn/error
    'svelte/valid-compile': ['error', { 'ignoreWarnings': true }],
    // Enforce indent in .svelte files
    'svelte/indent': [
      'error',
      { 
        'indent': 2,
        'switchCase': 1,
        'alignAttributesVertically': true
      }
    ]
  }
}