export function shorterText(text: string, maxLength: number): string {
  if (text.length <= maxLength) {
    return text
  }
  let lastSpace = 0;
  for(let i = 0 ; i < maxLength; i++) {
    if (text[i] === " ") {
      lastSpace = i;
    }
  }
  return text.slice(0, lastSpace) + "..."
}