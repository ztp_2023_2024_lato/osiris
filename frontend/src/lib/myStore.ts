import { writable } from "svelte/store";

export const familyRepaint = writable(0);
export const speciesRepaint = writable(0);