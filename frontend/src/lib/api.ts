import {speciesMock} from '@lib/mockData'
import type { FamilyResponse, SpeciesResponse, FamilyCreateRequest, SpeciesCreateRequest, LoginRequest } from './types';

const apiURL = import.meta.env.VITE_OSIRIS_API_URL || 'https://api.example.com/';
console.log("AAAAAAAAAAAAAAAAAAAAAA");
console.log(apiURL);
// ===== types

export type ParsedResponse<T> = {
    status: number;
    body: T;
    response: Response;
}

export type ApiResponse<T> = Promise<ParsedResponse<T>>;

// ===== functions

function mockToApiResponse<T>(obj: T) : ApiResponse<T> {
  return Promise.resolve({
    status: 200,
    body: obj,
    response: new Response()
  });
}


function joinPaths(base: string, path: string) {
  const endsWith = base.endsWith('/');
  const startsWith = path.startsWith('/');
  if (endsWith && startsWith) {
    return base + path.slice(1);
  } else if (!endsWith && !startsWith) {
    return base + '/' + path;
  } else {
    return base + path;
  }
}

function mapResponse(response : Response) : ApiResponse<any> {
  return response.text().then(textBody => {
    let body;
    try {
      body = JSON.parse(textBody)
    }catch {
      body = {};  
    }
    const obj = {
      status: response.status,
      body: body,
      response: response
    };
    if (response.status >= 400) {
      throw obj;
    }
    return obj;
  }).catch(error => {
    throw {
      status: response.status,
      body: null,
      response: response,
      error: error
    }
  });
}

function prepareHeaders(additional: {[key: string]: string}) {
  return {
    ...additional
  };
}

function apiRequest(path: string, method: string, body: object|null = null) : ApiResponse<any> {
  const hasBody = body !== null;

  const headers = prepareHeaders( hasBody ? {
    'Content-Type': 'application/json'
  } : {});

  return fetch(joinPaths(apiURL, path), {
    method: method,
    body: hasBody ? JSON.stringify(body) : null,
    headers: headers,
    credentials: 'include'
  })
    .then(mapResponse)
    .catch(error => {
      if ("status" in error && "response" in error) {
        throw error;
      }
      console.error('Error:', error);
    }) as ApiResponse<any>;
}


function apiGet(path: string) {
  return apiRequest(path, 'GET', null);
}

function apiPost(path: string, body: object) {
  return apiRequest(path, 'POST', body);
}

function apiPut(path: string, body: object) {
  return apiRequest(path, 'PUT', body);
}

function apiDelete(path: string) {
  return apiRequest(path, 'DELETE', null);
}

export function getSpecies() : ApiResponse<SpeciesResponse> {
  return apiGet('/species');
  // return mockToApiResponse(speciesMock);
}

export function getFamilies() : ApiResponse<FamilyResponse> {
  return apiGet('/family');
  // return mockToApiResponse(
  //     speciesMock
  //     .map(s => s.family)
  //     .filter((v, i, a) => a.map(x=>x.id).indexOf(v.id) === i));
}

export function addSpecies(species: SpeciesCreateRequest) : ApiResponse<Species> {
  return apiPost('/species', species);
}

export function deleteSpecies(id: string) : ApiResponse<any> {
  return apiDelete(`/species/${id}`);
}

export function addFamily(family: FamilyCreateRequest) : ApiResponse<Family> {
  return apiPost('/family', family);
}

export function login(loginRequest: LoginRequest) : ApiResponse<any> {
  return apiPost('/auth/login', loginRequest);
}