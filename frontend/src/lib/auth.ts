import { writable } from "svelte/store";

export interface Session {
    isLogged: boolean;
    name: string;
}

const session = writable({
  isLogged: false,
  name: ''
} as Session);

export function login(username :string) {
  console.log("login");
  session.update(s => {
    s.isLogged = true;
    s.name = username;
    return s;
  });
}

export function logout() {
  session.update(s => {
    s.isLogged = false;
    s.name = '';
    return s;
  });
}

export function subscribeAuthState(callback: (value: Session) => void) {
  return session.subscribe(callback);
}