export type FamilyResponse = {
    items: Family[];
}

export type Family = {
    id: string;
    name: string;
    description: string;
}

export type SpeciesResponse = {
    items: Species[];
}

export type Species = {
    id: string;
    name: string;
    description: string;
    family: Family;
}

export type SpeciesCreateRequest = {
    name: string;
    description: string;
    family: string;
}

export type FamilyCreateRequest = {
    name: string;
    description: string;
}

export type LoginRequest = {
    username: string;
    password: string;
}