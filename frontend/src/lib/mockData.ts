const speciesMock = [
  {
    "id": "2a59a575-d3df-42ee-adf8-f70f6295d9d2",
    "name": "Jukka gwatemalska",
    "description": "Rośnie zazwyczaj na półpustyniach wśród kaktusów. Pochodzi z Ameryki Środkowej i środkowego Meksyku, jest uprawiana wielu krajach świata.",
    "family": {
      "id": "c2c1b596-bdee-4ce8-9f66-47a63dc0095d",
      "name": "Szparagowate",
      "description": "W różnych systemach klasyfikacyjnych roślin do początków XXI wieku była wąsko definiowana, zwykle z jednym rodzajem – szparag (Asparagus)."
    }
  },
  {
    "id": "58ae38e9-be6b-49e6-88d4-f9d720d09e89",
    "name": "Yucca faxoniana",
    "description": "Najbardziej mrozoodporna jukka wytwarzająca tak duży pień. Wytrzymuje długie spadki temperatury do -18°C, a krótkotrwałe nawet do -23°C",
    "family": {
      "id": "c2c1b596-bdee-4ce8-9f66-47a63dc0095d",
      "name": "Szparagowate",
      "description": "W różnych systemach klasyfikacyjnych roślin do początków XXI wieku była wąsko definiowana, zwykle z jednym rodzajem – szparag (Asparagus)."
    }
  },
  {
    "id": "5c022a08-1c48-42bb-873b-e560b28d4e00",
    "name": "Rosiczka długolistna",
    "description": "Występuje w Azji, Europie i Ameryce Północnej. Roślina owadożerna.",
    "family": {
      "id": "b56789cb-2543-47de-8e49-aa5c47edbe82",
      "name": "Rosiczkowate",
      "description": "Rodzina roślin mięsożernych spokrewniona z kilkoma rodzinami także roślin mięsożernych."
    }
  },
  {
    "id": "69034bbd-62c2-4e73-a89f-0f0c35dcd02d",
    "name": "Rosiczka przerastająca",
    "description": "Występuje w lasach równikowych australijskiego stanu Queensland.",
    "family": {
      "id": "b56789cb-2543-47de-8e49-aa5c47edbe82",
      "name": "Rosiczkowate",
      "description": "Rodzina roślin mięsożernych spokrewniona z kilkoma rodzinami także roślin mięsożernych."
    }
  },
  {
    "id": "a4a8266c-5138-4795-bcbf-3f8325c419c0",
    "name": "Dracena wonna",
    "description": " Jest uprawiany jako roślina ozdobna, w uprawie znajdują się głównie kultywary dużo mniejsze od typowej, dziko rosnącej formy gatunku.",
    "family": {
      "id": "c2c1b596-bdee-4ce8-9f66-47a63dc0095d",
      "name": "Szparagowate",
      "description": "W różnych systemach klasyfikacyjnych roślin do początków XXI wieku była wąsko definiowana, zwykle z jednym rodzajem – szparag (Asparagus)."
    }
  }
];

export { speciesMock };