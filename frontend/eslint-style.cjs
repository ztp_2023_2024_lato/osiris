module.exports = {
  rules: {
    'no-var': 'error', // Use let, const
    'indent': ['error', 2], // Require indentation (2 spaces)
  }
}